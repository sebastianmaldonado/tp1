package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Moneda {
	
	private double x;
	private double y;
	private int ancho;
	private int alto;
	private Image[] img = new Image[4];
	
	private int tiempo;
	private int imgNumero;
	private double escala;
	
	public Moneda(double[] coordenadas) {
		this.x = coordenadas[0];
		this.y = coordenadas[1];
		this.ancho = 50;
		this.alto = 50;
		
		this.tiempo = 0;
		this.escala = 0.3;
		this.imgNumero = 0;
		
		// Cargar imagenes de la moneda en array
		for(int i = 0; i < img.length; i++) {
			this.img[i] = Herramientas.cargarImagen("img/moneda_" + (i + 1) + ".png");
		}

	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public int getAncho() {
		return ancho;
	}
	
	public int getAlto() {
		return alto;
	}

	
	public static void dibujar(Entorno e, Moneda moneda) {
		if(existe(moneda)) {
			moneda.dibujar(e);
		}
	}
	
	public void dibujar(Entorno e) {
		// Mientras este visible la ventana, el tiempo corre
		if(e.isEnabled()) {
			tiempo++;
		}
		
		// Animacion de moneda
		if(tiempo < 10) {
			imgNumero = 0;
		} else if(tiempo < 20) {
			imgNumero = 1;
		} else if(tiempo < 30) {
			imgNumero = 2;
		} else if(tiempo < 40) {
			imgNumero = 3;
		} else {
			tiempo = 0;
		}
		e.dibujarImagen(img[imgNumero], x, y, 0, escala);
	}	
	
	public boolean agarradaPor(Jugador jugador) {
		return jugador.getX() > x - ancho/2 - 20 &&
				jugador.getY() > y - ancho/2 - 10 && 
				jugador.getX() < x + ancho/2 + 10 &&
				jugador.getY() < y + ancho/2 + 10;
	}
	
	public static boolean existe(Moneda moneda) {
		return moneda != null;
	}
	

	
	public static double[] nuevaUbicacion(Pared[] paredes) {
		// Obtiene una pared al azar de paredes
		int paredRandom = (int) (Math.random()*paredes.length);
		
		// Busca una pared que no esten en los bordes inferiores y derechos del juego
		while(paredes[paredRandom].getFila() > 2 || paredes[paredRandom].getColumna() > 2) {
			paredRandom = (int) (Math.random()*paredes.length);
		}
		
		// Guarda coordenadas de lugar de aparicion[x, y] y lo devuelve 
		double[] coordenadas = new double[2];
		coordenadas[0] = paredes[paredRandom].getX() + 136;
		coordenadas[1] = paredes[paredRandom].getY() + paredes[paredRandom].getAlto() - 14;
		return coordenadas;
	}
}