package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Jugador {
	
	// Configuracion de jugador
	private double x;
	private double y;
	private int ancho;
	private int alto;
	private double velocidad;
	
	// Configuracion de imagenes
	private Image[] img = new Image[6];
	private int tiempo;
	private int imgNumero;
	private double escala;
	
	public Jugador(double x, double y) {
		this.x = x;
		this.y = y;
		this.ancho = 50;
		this.alto = 50;
		this.velocidad = 1.5;
		
		this.tiempo = 0;
		this.escala = 0.2;
		this.imgNumero = 0;
		
		// Cargar imagenes del jugador en array
		for(int i = 0; i < img.length; i++) {
			this.img[i] = Herramientas.cargarImagen("img/marioCaminando_" + (i + 1) + ".png");
		}
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public int getAncho() {
		return ancho;
	}
	
	public int getAlto() {
		return alto;
	}
	
	public static boolean existe(Jugador jugador) {
		return jugador != null;
	}
	
	public static void dibujar(Entorno e, Jugador jugador) {
		if(existe(jugador)) {
			jugador.dibujar(e);
		}
	}
	
	public void dibujar(Entorno e) {
		// Mientras este visible la ventana, el tiempo corre
		if(e.isEnabled()) {
			tiempo++;
		}
		
		// Animacion de jugador caminando
		if(e.estaPresionada('d') && !e.estaPresionada('a')) {
			if(tiempo < 10) {
				imgNumero = 0;
			} else if(tiempo < 20) {
				imgNumero = 1;
			} else if(tiempo < 30) {
				imgNumero = 2;
			} else {
				tiempo = 0;
			}
		} else if(e.estaPresionada('a') && !e.estaPresionada('d')) {
			if(tiempo < 10) {
				imgNumero = 3;
			} else if(tiempo < 20) {
				imgNumero = 4;
			} else if(tiempo < 30) {
				imgNumero = 5;
			} else {
				tiempo = 0;
			} 
		}
		e.dibujarImagen(img[imgNumero], x, y, 0, escala);
	}
	
	
	// Mueve al jugador hacia arriba, abajo, izquierda o derecha	
	public void mover(String direccion) {		
		if(direccion.equals("arriba")) {
			y -= velocidad;
		}
		
		if(direccion.equals("abajo")) {
			y += velocidad;
		}
		
		if(direccion.equals("izquierda")) {
			x -= velocidad;
		}
		
		if(direccion.equals("derecha")) {
			x += velocidad;
		}
	}
	
	

	// -------- Verifica si existe colision de jugador con pared -------------
	// Cada condicion (en orden descendente) corresponde a los lados de la pared, 
	// donde al jugador se le impide moverse (Arriba, abajo, izquierda, derecha)
	public boolean colisionArribaConPared(Pared pared) {
		return y > pared.getY() - 76 && 
				y < pared.getY() + pared.getAlto() - 28 && 
				x > pared.getX() - 120 && 
				x < pared.getX() + pared.getAncho() - 75;
	}
	
	public boolean colisionAbajoConPared(Pared pared) {
		return y > pared.getY() - 74 && 
				y < pared.getY() + pared.getAlto() - 26 && 
				x > pared.getX() - 120 &&
				x < pared.getX() + pared.getAncho() - 75;
	}
	
	public boolean colisionIzquierdaConPared(Pared pared) {
		return y > pared.getY() - 74 && 
				y < pared.getY() + pared.getAlto() - 28 && 
				x > pared.getX() - 125 && 
				x < pared.getX() + pared.getAncho() - 75;
	}

	public boolean colisionDerechaConPared(Pared pared) {
		return y > pared.getY() - 74 && 
				y < pared.getY() + pared.getAlto() - 28 && 
				x > pared.getX() - 120 && 
				x < pared.getX() + pared.getAncho() - 72;
	}
	
	
	// True: si el jugador esta en el borde de la ventana
	public boolean enBordeSuperiorVentana(Entorno e) {
		int margenSuperior = 65;
		return y > margenSuperior;
	}
	
	public boolean enBordeInferiorVentana(Entorno e) {
		int margenInferior = 65;
		return y < e.getHeight() - margenInferior;
	}
	
	public boolean enBordeIzquierdoVentana(Entorno e) {
		int margenIzquierdo = 25;
		return x > margenIzquierdo;
	}
	
	public boolean enBordeDerechoVentana(Entorno e) {
		int margenDerecho = 40;
		return x < e.getWidth() - margenDerecho;
	}
}
