package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Enemigo {
	
	private double x;
	private double y;
	private int ancho;
	private int alto;
	private double velocidad;
	private String movimiento;
	private Image[] img = new Image[2];
	
	private int tiempo;
	private int imgNumero;
	private double escala;
	
	public Enemigo(double[] coordenadas, String movimiento) {
		this.x = coordenadas[0];
		this.y = coordenadas[1];
		this.ancho = 50;
		this.alto = 50;
		this.velocidad = 1.5;
		this.movimiento = movimiento;
		
		this.tiempo = 0;
		this.escala = 0.3;
		this.imgNumero = 0;
		
		// Cargar imagenes de la moneda en array
		for(int i = 0; i < img.length; i++) {
			this.img[i] = Herramientas.cargarImagen("img/goomba_" + (i + 1) + ".png");
		}
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public double getAncho() {
		return ancho;
	}
	
	public double getAlto() {
		return alto;
	}
	
	public String getMovimiento() {
		return movimiento;
	}
	
	public static boolean existe(Enemigo enemigo) {
		return enemigo != null;
	}
	
	public static void dibujar(Entorno e, Enemigo enemigo) {
		if(existe(enemigo)) {
			enemigo.dibujar(e);
		}
	}
	
	public void dibujar(Entorno e) {
		// Mientras este visible la ventana, el tiempo corre
		if(e.isEnabled()) {
			tiempo++;
		}
		
		// Animacion de moneda
		if(tiempo < 25) {
			imgNumero = 0;
		} else if(tiempo < 50) {
			imgNumero = 1;
		} else {
			tiempo = 0;
		}
		e.dibujarImagen(img[imgNumero], x, y, 0, escala);
	}
	
	public void mover() {
		if(movimiento.equals("arriba")) {
			y -= velocidad;
		}
		
		if(movimiento.equals("abajo")) {
			y += velocidad;
		}
		
		if(movimiento.equals("izquierda")) {
			x -= velocidad;
		}
		
		if(movimiento.equals("derecha")) {
			x += velocidad;
		}	
	}
	
	public void teletransportar(Entorno e, String direccion) {
		if(direccion.equals("arriba")) {
			y = -this.alto;
		}
		
		if(direccion.equals("abajo")) {
			y = e.getHeight();
		}
		
		if(direccion.equals("izquierda")) {
			x = -this.ancho;
		}
		
		if(direccion.equals("derecha")) {
			x = e.getWidth();
		}
	}
	
	public boolean colisionConJugador(Jugador jugador) {
		return y + alto > jugador.getY() &&
				y < jugador.getY() + jugador.getAlto() &&
				x + ancho > jugador.getX() &&
				x < jugador.getX() + jugador.getAncho();
	}
	
	// True: si el enemigo esta en el borde de la ventana
	public boolean enBordeSuperiorVentana(Entorno e) {
		int margenSuperior = 65;
		return y < -margenSuperior;
	}
	
	public boolean enBordeInferiorVentana(Entorno e) {
		int margenInferior = 65;
		return y > e.getHeight() + margenInferior;
	}
	
	public boolean enBordeIzquierdoVentana(Entorno e) {
		int margenIzquierdo = 25;
		return x < -margenIzquierdo;
	}
	
	public boolean enBordeDerechoVentana(Entorno e) {
		int margenDerecho = 40;
		return x > e.getWidth() + margenDerecho;
	}
	
	public static double[] aparecerDerechaDe(Pared pared) {
		// Guarda coordenadas de lugar de aparicion[x, y] y lo devuelve 
		double[] coordenadas = new double[2];
		coordenadas[0] = pared.getX() + 136;
		coordenadas[1] = pared.getY();
		return coordenadas;
	}
	
	public static double[] aparecerAbajoDe(Pared pared) {
		// Guarda coordenadas de lugar de aparicion[x, y] y lo devuelve 
		double[] coordenadas = new double[2];
		if(pared.getX() <= 0) {
			coordenadas[0] = pared.getX() + 50;	
		} else {
			coordenadas[0] = pared.getX() - 50;
		}
		coordenadas[1] = pared.getY() + 80;
		return coordenadas;
	}
}
