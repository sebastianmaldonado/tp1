package juego;

import java.awt.Image;
import entorno.Entorno;
import entorno.Herramientas;

public class Pared {

	private double x;
	private double y;
	private int ancho;
	private int alto;
	private int fila;
	private int columna;
	
	private Image manz1;
	private Image manz2;

	public Pared(double x, double y, int fila, int columna) {
		this.x = x;
		this.y = y;
		this.ancho = 200;
		this.alto = 100;
		this.fila = fila;
		this.columna = columna;

		manz1=Herramientas.cargarImagen("img/manz1.png");
		manz2=Herramientas.cargarImagen("img/manz2.png");
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public int getAncho() {
		return ancho;
	}

	public int getAlto() {
		return alto;
	}

	public int getFila() {
		return fila;
	}

	public int getColumna() {
		return columna;
	}

	public void dibujar1(Entorno e) {
		e.dibujarImagen(manz1, this.x, this.y, 0);
	}

	public void dibujar2(Entorno e) {
		e.dibujarImagen(manz2, this.x, this.y, 0);
	}
}
