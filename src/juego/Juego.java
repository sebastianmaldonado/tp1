package juego;

import java.awt.Color;
import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;
import entorno.InterfaceJuego;

public class Juego extends InterfaceJuego {

	// El objeto Entorno que controla el tiempo y otros
	private Entorno entorno;

	private boolean jugadorExiste;
	private boolean objetivoExiste;

	private Jugador jugador;
	private Proyectil proyectil;
	private Pared[] paredes;
	private Image calle;
	private Moneda moneda;
	private Enemigo[] enemigos;
	private Objetivo objetivo;

	private int puntaje;
	private int puntajeObjetivo;
	private int enemigosEliminados;
	private double[] nuevaPosicion;
	private double[] ultimaPosicion;

	private int intervaloAparicionMoneda;
	private int intervaloAparicionEnemigo;
	private int intervaloProyectilListo;

	private boolean sePuedeMoverArriba;
	private boolean sePuedeMoverAbajo;
	private boolean sePuedeMoverIzquierda;
	private boolean sePuedeMoverDerecha;
	private String jugadorUltimoMovimiento;

	public Juego() {
		// Inicializa el objeto entorno
		this.entorno = new Entorno(this, "Mario Delivery v1.0", 800, 600);

		// Inicializar jugador
		jugador = new Jugador(405, 345);
		jugadorExiste = true;
		objetivoExiste = false;

		// Inicializar estadisticas
		puntaje = 0;
		puntajeObjetivo = 100;
		enemigosEliminados = 0;

		// Inicializar tiempo de objetos
		intervaloAparicionMoneda = 0;
		intervaloAparicionEnemigo = 0;
		intervaloProyectilListo = 500;

		// Inicializar paredes
		int columnas = 4;
		int filas = 4;
		int margenSuperior = 80;
		int anchoPared = 270;
		int largoPared = 170;
		paredes = new Pared[columnas * filas];
		for (int c = 0, i = 0; c < columnas; c++) {
			for (int f = 0; f < filas; f++) {
				paredes[i] = new Pared((c * anchoPared), ((f + 1) * largoPared) - margenSuperior, c, f);
				i++;
			}
		}
		// Inicializar selector
		objetivo = new Objetivo(Objetivo.nuevaUbicacion(paredes));
		nuevaPosicion = new double[2];
		ultimaPosicion = new double[2];
		ultimaPosicion[0] = objetivo.getX();
		ultimaPosicion[1] = objetivo.getY();

		
		// Inicializar enemigos
		enemigos = new Enemigo[6];
		enemigos[0] = new Enemigo(Enemigo.aparecerDerechaDe(paredes[0]), "abajo");
		enemigos[1] = new Enemigo(Enemigo.aparecerDerechaDe(paredes[5]), "arriba");
		enemigos[2] = new Enemigo(Enemigo.aparecerDerechaDe(paredes[10]), "abajo");
		enemigos[3] = new Enemigo(Enemigo.aparecerAbajoDe(paredes[0]), "izquierda");
		enemigos[4] = new Enemigo(Enemigo.aparecerAbajoDe(paredes[5]), "izquierda");
		enemigos[5] = new Enemigo(Enemigo.aparecerAbajoDe(paredes[10]), "derecha");

		// Precargar multimedia
		calle = Herramientas.cargarImagen("img/calles.png");
		Herramientas.cargarSonido("sound/monedaAparece.wav");
		Herramientas.cargarSonido("sound/monedaAgarrada.wav");
		Herramientas.cargarSonido("sound/bolaFuego.wav");
		Herramientas.cargarImagen("img/moneda_1.png");
		Herramientas.cargarImagen("img/moneda_2.png");
		Herramientas.cargarImagen("img/moneda_3.png");
		Herramientas.cargarImagen("img/moneda_4.png");
		Herramientas.cargarImagen("img/bolaFuego.png");

		// Inicia el juego!
		this.entorno.iniciar();
	}

	public void tick() {
		// ------------- Dibujar calles -----------------------------------
		entorno.dibujarImagen(calle, entorno.getWidth() / 2 - 10, entorno.getHeight() / 2 - 20, 0);

		// ------------- Dibujar Enemigos eliminados ----------------------
		int margenIzquierdo = 15;
		entorno.cambiarFont("Consolas", 18, Color.WHITE);
		entorno.escribirTexto("Enemigos eliminados: " + enemigosEliminados, margenIzquierdo, 25);

		// ------------- Dibujar Puntaje ----------------------------------
		int margenDerecho = 185;
		entorno.cambiarFont("Consolas", 18, Color.WHITE);
		entorno.escribirTexto("Puntaje: " + puntaje + "/" + puntajeObjetivo, entorno.getWidth() - margenDerecho, 25);

		// ------------- Dibujar paredes ----------------------------------
		for (int i = 0; i < paredes.length; i++) {
			if (i >= 4 && i < 8 || i >= 12 && i < 16) {
				if (i % 2 == 0) {
					paredes[i].dibujar2(entorno);
				} else {
					paredes[i].dibujar1(entorno);
				}
			} else if (i < 4 || i >= 8 && i < 12)
				if (i % 2 == 0) {
					paredes[i].dibujar1(entorno);
				} else {
					paredes[i].dibujar2(entorno);
				}

		}
		

		if (jugadorExiste) {

			jugador.dibujar(entorno);
			
		// ------------- Dibujar objetivo ----------------------------------	
			
			if (Objetivo.existe(objetivo)) {
				objetivo.dibujar(entorno);
				ultimaPosicion[0] = objetivo.getX();
				ultimaPosicion[1] = objetivo.getY();
				if (objetivo.colisionaCon(jugador)) {
					puntaje+=10;
					objetivo = null;
				}
			} else if(!Objetivo.existe(objetivo)) {
				nuevaPosicion = Objetivo.nuevaUbicacion(paredes); 
				while (ultimaPosicion[0] == nuevaPosicion[0] && ultimaPosicion[1] == nuevaPosicion[1]) {
					nuevaPosicion[0] = Objetivo.nuevaUbicacion(paredes)[0];
					nuevaPosicion[1] = Objetivo.nuevaUbicacion(paredes)[1];
					
				}
				
				objetivo = new Objetivo(nuevaPosicion);
			}

			// ------------- Conteo individual de intervalos ------------------
			if (intervaloAparicionMoneda < 500) {
				intervaloAparicionMoneda++;
			} else {
				intervaloAparicionMoneda = 0;
			}

			if (intervaloAparicionEnemigo < 1500) {
				intervaloAparicionEnemigo++;
			} else {
				intervaloAparicionEnemigo = 0;
			}

			if (intervaloProyectilListo < 500) {
				intervaloProyectilListo++;
			}

			// ------------- Generar moneda ----------------------------------
			if (Moneda.existe(moneda)) {
				moneda.dibujar(entorno);
			}

			if (Moneda.existe(moneda)) {
				if (moneda.agarradaPor(jugador)) {
					Herramientas.cargarSonido("sound/monedaAgarrada.wav").start();
					puntaje += 5;
					moneda = null;
				}
			} else if (intervaloAparicionMoneda == 500) {
				moneda = new Moneda(Moneda.nuevaUbicacion(paredes));
				Herramientas.cargarSonido("sound/monedaAparece.wav").start();
			} else if (intervaloAparicionMoneda > 500) {
				intervaloAparicionMoneda = 0;
			}

			// ------------- Dibuja enemigos ----------------------------------
			for (int i = 0; i < enemigos.length; i++) {
				if (Enemigo.existe(enemigos[i])) {
					enemigos[i].dibujar(entorno);

					if (enemigos[i].getMovimiento().equals("arriba")) {
						if (!enemigos[i].enBordeSuperiorVentana(entorno)) {
							enemigos[i].mover();
						} else {
							enemigos[i].teletransportar(entorno, "abajo");
						}
					}

					if (enemigos[i].getMovimiento().equals("abajo")) {
						if (!enemigos[i].enBordeInferiorVentana(entorno)) {
							enemigos[i].mover();
						} else {
							enemigos[i].teletransportar(entorno, "arriba");
						}
					}

					if (enemigos[i].getMovimiento().equals("izquierda")) {
						if (!enemigos[i].enBordeIzquierdoVentana(entorno)) {
							enemigos[i].mover();
						} else {
							enemigos[i].teletransportar(entorno, "derecha");
						}
					}

					if (enemigos[i].getMovimiento().equals("derecha")) {
						if (!enemigos[i].enBordeDerechoVentana(entorno)) {
							enemigos[i].mover();
						} else {
							enemigos[i].teletransportar(entorno, "izquierda");
						}
					}

					if (enemigos[i].colisionConJugador(jugador)) {
						jugadorExiste = false;
					}

				} else if (intervaloAparicionEnemigo >= 750) {
					if (i == 0) {
						enemigos[0] = new Enemigo(Enemigo.aparecerDerechaDe(paredes[0]), "abajo");
					} else if (i == 1) {
						enemigos[1] = new Enemigo(Enemigo.aparecerDerechaDe(paredes[5]), "arriba");
					} else if (i == 2) {
						enemigos[2] = new Enemigo(Enemigo.aparecerDerechaDe(paredes[10]), "abajo");
					} else if (i == 3) {
						enemigos[3] = new Enemigo(Enemigo.aparecerAbajoDe(paredes[0]), "izquierda");
					} else if (i == 4) {
						enemigos[4] = new Enemigo(Enemigo.aparecerAbajoDe(paredes[5]), "izquierda");
					} else if (i == 5) {
						enemigos[5] = new Enemigo(Enemigo.aparecerAbajoDe(paredes[10]), "derecha");
					}
					intervaloAparicionEnemigo = 0;
				}

			}

			// ------------- Verifica posibilidad de movimiento ---------------
			// False: Si el jugador choco con alguna pared, bloquea el movimiento en dicha
			// direccion.
			for (int i = 0; i < paredes.length; i++) {
				if (jugador.colisionAbajoConPared(paredes[i])) {
					sePuedeMoverArriba = false;
					break;
				} else {
					sePuedeMoverArriba = true;
				}

				if (jugador.colisionArribaConPared(paredes[i])) {
					sePuedeMoverAbajo = false;
					break;
				} else {
					sePuedeMoverAbajo = true;
				}

				if (jugador.colisionDerechaConPared(paredes[i])) {
					sePuedeMoverIzquierda = false;
					break;
				} else {
					sePuedeMoverIzquierda = true;
				}

				if (jugador.colisionIzquierdaConPared(paredes[i])) {
					sePuedeMoverDerecha = false;
					break;
				} else {
					sePuedeMoverDerecha = true;
				}
			}

			// ------------ Controles ----------------------------------
			if (entorno.estaPresionada('w') && sePuedeMoverArriba && jugador.enBordeSuperiorVentana(entorno)) {
				jugador.mover("arriba");
				if (!Proyectil.existe(proyectil) && entorno.estaPresionada(entorno.TECLA_ESPACIO)
						&& !entorno.estaPresionada('s') && intervaloProyectilListo == 500) {
					proyectil = new Proyectil(jugador);
					Herramientas.cargarSonido("sound/bolaFuego.wav").start();
					jugadorUltimoMovimiento = "arriba";
					intervaloProyectilListo = 0;
				}
			}

			if (entorno.estaPresionada('s') && sePuedeMoverAbajo && jugador.enBordeInferiorVentana(entorno)) {
				jugador.mover("abajo");
				if (!Proyectil.existe(proyectil) && entorno.estaPresionada(entorno.TECLA_ESPACIO)
						&& !entorno.estaPresionada('w') && intervaloProyectilListo == 500) {
					proyectil = new Proyectil(jugador);
					Herramientas.cargarSonido("sound/bolaFuego.wav").start();
					jugadorUltimoMovimiento = "abajo";
					intervaloProyectilListo = 0;
				}
			}

			if (entorno.estaPresionada('a') && sePuedeMoverIzquierda && jugador.enBordeIzquierdoVentana(entorno)) {
				jugador.mover("izquierda");
				if (!Proyectil.existe(proyectil) && entorno.estaPresionada(entorno.TECLA_ESPACIO)
						&& !entorno.estaPresionada('d') && intervaloProyectilListo == 500) {
					proyectil = new Proyectil(jugador);
					Herramientas.cargarSonido("sound/bolaFuego.wav").start();
					jugadorUltimoMovimiento = "izquierda";
					intervaloProyectilListo = 0;
				}
			}

			if (entorno.estaPresionada('d') && sePuedeMoverDerecha && jugador.enBordeDerechoVentana(entorno)) {
				jugador.mover("derecha");
				if (!Proyectil.existe(proyectil) && entorno.estaPresionada(entorno.TECLA_ESPACIO)
						&& !entorno.estaPresionada('a') && intervaloProyectilListo == 500) {
					proyectil = new Proyectil(jugador);
					Herramientas.cargarSonido("sound/bolaFuego.wav").start();
					jugadorUltimoMovimiento = "derecha";
					intervaloProyectilListo = 0;
				}
			}

			// -------------------- Proyectil --------------------
			if (Proyectil.existe(proyectil) && proyectil.estaVisibleEn(entorno)) {
				proyectil.dibujar(entorno);
				proyectil.lanzar(jugadorUltimoMovimiento);
				for (int i = 0; i < enemigos.length; i++) {
					if (Enemigo.existe(enemigos[i]) && proyectil.colisionConEnemigo(enemigos[i])) {
						enemigosEliminados++;
						enemigos[i] = null;
						proyectil = null;
						break;
					}
				}
			} else {
				proyectil = null;
			}

			// Elimina al jugador si se completo objetivo
			if (puntaje >= puntajeObjetivo) {
				jugador = null;
				jugadorExiste = false;
			}

		} else {
			// Suceso luego de dejar de existir jugador
			entorno.dibujarRectangulo(entorno.getWidth() / 2, entorno.getHeight() / 2 + 20, 500, 50, 0, Color.BLACK);

			if (puntaje >= puntajeObjetivo) {
				puntaje = puntajeObjetivo;
				entorno.cambiarFont("Consolas", 55, Color.WHITE);
				entorno.escribirTexto("GANASTE!!!", entorno.getWidth() / 2 - 140, entorno.getHeight() / 2 + 40);
			} else {
				entorno.cambiarFont("Consolas", 55, Color.WHITE);
				entorno.escribirTexto("GAME OVER", entorno.getWidth() / 2 - 140, entorno.getHeight() / 2 + 40);
			}

			entorno.cambiarFont("Consolas", 18, Color.WHITE);
			entorno.escribirTexto("Pulse [R] para jugar de nuevo", 275, 25);

			if (!jugadorExiste && entorno.estaPresionada('r')) {
				// Restablecer jugador, enemigos, moneda y estadisticas
				jugador = new Jugador(405, 345);
				jugadorExiste = true;

				moneda = null;
				proyectil = null;

				puntaje = 0;
				enemigosEliminados = 0;
				intervaloAparicionEnemigo = 0;
				intervaloProyectilListo = 500;

				for (int i = 0; i < enemigos.length; i++) {
					enemigos[i] = null;
				}

				enemigos[0] = new Enemigo(Enemigo.aparecerDerechaDe(paredes[0]), "abajo");
				enemigos[1] = new Enemigo(Enemigo.aparecerDerechaDe(paredes[5]), "arriba");
				enemigos[2] = new Enemigo(Enemigo.aparecerDerechaDe(paredes[10]), "abajo");
				enemigos[3] = new Enemigo(Enemigo.aparecerAbajoDe(paredes[0]), "izquierda");
				enemigos[4] = new Enemigo(Enemigo.aparecerAbajoDe(paredes[5]), "izquierda");
				enemigos[5] = new Enemigo(Enemigo.aparecerAbajoDe(paredes[10]), "derecha");
			}
		}
	}

	@SuppressWarnings("unused")
	public static void main(String[] args) {
		Juego juego = new Juego();
	}

}
