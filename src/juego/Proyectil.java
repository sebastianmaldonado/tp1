package juego;

import java.awt.Image;

import entorno.Entorno;
import entorno.Herramientas;

public class Proyectil {
	
	private double x;
	private double y;
	private int ancho;
	private int alto;
	private double velocidad;
	
	private double tiempo;
	private double escala;
	private Image img;
	
	public Proyectil(Jugador jugador) {
		this.x = jugador.getX();
		this.y = jugador.getY();
		this.ancho = 25;
		this.alto = 25;
		this.velocidad = 2.5;
		
		this.tiempo = 10;
		this.escala = 0.07;
		this.img = Herramientas.cargarImagen("img/bolaFuego.png");
	}
	
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}
	
	public int getAncho() {
		return ancho;
	}
	
	public int getAlto() {
		return alto;
	}
	
	public static boolean existe(Proyectil proyectil) {
		return proyectil != null;
	}
	
	public void dibujar(Entorno e) {
		tiempo += 0.5;
		e.dibujarImagen(img, x, y, tiempo, escala);
	}
	
	// Lanza al proyectil hacia arriba, abajo, izquierda o derecha	
	public void lanzar(String direccion) {
		if(direccion.equals("arriba")) {
			y -= velocidad;
		}
		
		if(direccion.equals("abajo")) {
			y += velocidad;
		}
		
		if(direccion.equals("izquierda")) {
			x -= velocidad;
		}
		
		if(direccion.equals("derecha")) {
			x += velocidad;
		}
	}
	
	public boolean colisionConEnemigo(Enemigo enemigo) {
		return y + alto > enemigo.getY() &&
				y < enemigo.getY() + enemigo.getAlto() &&
				x + ancho > enemigo.getX() &&
				x < enemigo.getX() + enemigo.getAncho();
	}
	
	// True: Si el proyectil esta dentro del entorno(ventana)
	public boolean estaVisibleEn(Entorno e) {
		return y > -20 &&
				y < e.getHeight() &&
				x > -20 &&
				x < e.getWidth();
	}
}
