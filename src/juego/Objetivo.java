package juego;

import entorno.Entorno;

public class Objetivo {

	private double x;
	private double y;
	private double ancho;
	private double alto;



	public Objetivo(double[] coordenadas) {
		
		this.x = coordenadas[0];
		this.y = coordenadas[1];
		

	}
	public double getX() {
		return x;
	}
	
	public double getY() {
		return y;
	}

//	public static double[] elegirCasa(Pared[] paredes) {
//		// Obtiene una casa al azar del mapa
		
		
		

//	}

	public void dibujar(Entorno e) {
		this.ancho = 30;
		this.alto = 30;
		e.dibujarRectangulo(x, y, ancho, alto, 0, null);
	}

	public boolean colisionaCon(Jugador jugador) {
		return jugador.getX() > x - ancho / 2 - 20 && 
				jugador.getY() > y - ancho / 2 - 10	&& 
				jugador.getX() < x + ancho / 2 + 10 && 
				jugador.getY() < y + ancho / 2 + 10;
	}

	public static double[] nuevaUbicacion(Pared[] paredes) {
		// Obtiene una pared al azar de paredes
		int paredRandom = (int) (Math.random() * paredes.length);

		// Busca una pared que no esten en los bordes inferiores y derechos del juego
		while (paredes[paredRandom].getFila() > 2 || paredes[paredRandom].getColumna() > 2) {
			paredRandom = (int) (Math.random() * paredes.length);
		}

		// Guarda coordenadas de lugar de aparicion[x, y] y lo devuelve
		double[] coordenadas = new double[2];
		coordenadas[0] = paredes[paredRandom].getX() + 45;
		coordenadas[1] = paredes[paredRandom].getY() + paredes[paredRandom].getAlto() - 14;
		return coordenadas;
	}
	public static boolean existe(Objetivo objetivo) {		
		return objetivo != null;
	}
	
}